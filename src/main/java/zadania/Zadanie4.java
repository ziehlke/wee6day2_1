package zadania;

// Napisz metodę, która przyjmuje dowolną liczbę liczb typu double (varargs) i
// tworzy zbiór (HashSet), składający się z tych elementów, lub wyrzuca IllegalArgumentException,
// jeżeli elementy nie są unikalne
//          public static Set<Double> newSet(double numbers…)
//          spróbuj przerobić metodę na wersję generyczną
//          public static <T> Set<T> newSet(T... data)


import java.util.HashSet;
import java.util.Set;

public class Zadanie4 {
    public static void main(String[] args) {

        createHashSetofDouble(2.3,3.9,6.4,66.5);
        createHashSetofDouble(2.3,2.3,6.4,66.5);
    }

    public static HashSet<Double> createHashSetofDouble(Double... doubles){

        Set<Double> set = new HashSet<>();

        for (Double d: doubles
             ) {

            if (set.contains(d)) {
                throw new IllegalArgumentException();
            }
            else {
                set.add(d);
            }
        }

        System.out.println("Set has been created: " + set);
        return new HashSet<>(set);
    }
}
