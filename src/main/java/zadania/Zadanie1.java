package zadania;


import java.util.*;

public class Zadanie1 {
//Umieść wszystkie elementy tablicy {10,12,10,3,4,12,12,300,12,40,55} w zbiorze (HashSet) oraz:


    public static void main(String[] args) {
        Set<Integer> set = new TreeSet<>(Arrays.asList(10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55));

        Set<Integer> linkedSet = new LinkedHashSet<>();
        Set<Integer> setTree = new TreeSet<>();

//        Wypisz liczbę elementów na ekran (metoda size())
        System.out.println("set size: ");
        System.out.println(set.size());
//        Wypisz wszystkie zbioru elementy na ekran (forEach)

        System.out.println("wszystkie elementy: ");
        for (Integer i : set
        ) {
            System.out.println(i);
        }

//        Usuń elementy 10 i 12, wykonaj ponownie podpunkty a) i b)
        System.out.println("usuwam 10 i 12");
        set.remove(10);
        set.remove(12);
        System.out.println("set size: ");
        System.out.println(set.size());

        System.out.println("jeszcze raz wsyztie elemnety");
        for (Integer i : set
        ) {
            System.out.println(i);
        }

//        zmień implementacje na TreeSet


    }
}
