package zadania;


/*Stwórz aplikację która przyjmuje od użytkownika ciąg znaków (dowolny).
        Podziel ciąg (split) na pojedyncze litery. Twoim zadaniem jest stworzenie aplikacji
        która wypisze tylko unikalne litery frazy wpisanej przez użytkownika.
        Pomyśl o wykorzystaniu cechy zbioru - pamiętaj, że zbiór sam usuwa duplikaty.

        Zadanie 1b:
        Stwórz aplikację która dzieli zdanie na słowa, a następnie wypisuje tylko unikalne*/


import java.util.*;

public class Zadanie1b {


    public static void main(String[] args) {

        System.out.println("Podaje znadnie: ");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        List<String> slowa = Arrays.asList(text.toLowerCase().split(" "));
        System.out.println(slowa);


        Set<String> bezDuplikatow = new HashSet<>(slowa);
        System.out.println("Unikalne slowa to:" + bezDuplikatow);


        // TODO: podobnie jak zliczanie ilosci jezykow... w Stream / Lambdas / Main
//        Map<String, Long> slowoPowtorzenie = slowa.stream()
//                .collect(Collectors.groupingBy(p -> p, LinkedHashMap::new, Collectors.counting()));

    }
}
