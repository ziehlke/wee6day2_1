package zadania;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Zadanie2 {

/*    Napisz statyczną metodę sprawdzającą,
    czy w tekście nie powtarzają się litery z wykorzystaniem zbioru.
        (boolean containDuplicates(String text))*/

    public static void main(String[] args) {

        System.out.println(containDuplicates("jakis text do sprawdzenia"));

    }

    public static boolean containDuplicates(String text) {

        List<Character> list = text.
                chars()
                .mapToObj(e -> ((char) e))
                .collect(Collectors.toList());

        System.out.println(list);
        Set<Character> set = new HashSet<>(list);


        if (set.size() == list.size()) return false;
        return true;
    }


    public static boolean containDuplicates2(String text) {
        Set<Character> characterSet = new HashSet<>();
        for (char c : text.toCharArray()) {
            characterSet.add(c);
        }

        return characterSet.size() == text.length();
    }

}
