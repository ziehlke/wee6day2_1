package zadania;

/*Utwórz klasę ParaLiczb (int a,int b) i dodaj kilka instancji do zbioru:
        {(1,2), (2,1), (1,1), (1,2)}.
        Wyświetl wszystkie elementy zbioru na ekran. Czy program działa zgodnie z oczekiwaniem?*/


import java.util.HashSet;
import java.util.Set;

public class Zadanie3 {

    public static void main(String[] args) {


        ParaLiczb paraLiczb = new ParaLiczb(1,2);
        ParaLiczb paraLiczb2 = new ParaLiczb(2,1);
        ParaLiczb paraLiczb3 = new ParaLiczb(1,1);
        ParaLiczb paraLiczb4 = new ParaLiczb(1,2);


        Set<ParaLiczb> set = new HashSet<>();

        set.add(paraLiczb);
        set.add(paraLiczb2);
        set.add(paraLiczb3);
        set.add(paraLiczb4);

        System.out.println(set);

    }

}
